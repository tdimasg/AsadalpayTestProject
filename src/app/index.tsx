import React, { useEffect } from 'react';
import { ReactQueryDevtools } from 'react-query/devtools';
import { withProviders } from 'app/providers';
import { Routing } from 'pages';
import AOS from 'aos';
import 'aos/dist/aos.css';
import Layout from './layout';
import './styles/reset.css';

const App = () => {
  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <Layout>
      <Routing />
      <ReactQueryDevtools/>
    </Layout>
  );
};

export default withProviders(App);
