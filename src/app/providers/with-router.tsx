import React, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';

export const withRouter = function withRouter(component: () => React.ReactNode) {
  function WithRouter() {
    return (
      <BrowserRouter>
        <Suspense fallback={<p>...loading</p>}>{component()}</Suspense>
      </BrowserRouter>
    );
  }

  WithRouter.displayName = `WithRouter(${component.name || 'Component'})`;

  return WithRouter;
};
