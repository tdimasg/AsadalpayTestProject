import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

export const withQuery = function withQuery(component: () => React.ReactNode) {
  function WithQuery() {
    return (
      <QueryClientProvider client={queryClient} contextSharing>
        {component()}
      </QueryClientProvider>
    );
  }

  WithQuery.displayName = `WithQuery(${component.name || 'Component'})`;
  return WithQuery;
};
