
import { Header } from 'antd/es/layout/layout';

interface MyComponentProps {
    children: React.ReactNode;
  }

const Layout: React.FC<MyComponentProps> = ({ children })=> {

  return (
    <>
    <Header />
    {children}
    </>
  );
};

export default Layout;
