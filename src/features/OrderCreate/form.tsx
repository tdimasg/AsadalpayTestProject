import React from 'react';
import {
  Col,
  Row,
  Input,
  Form,
  DatePicker,
  Button,
  Typography,
  Tooltip,
} from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const OrderCreateForm: React.FC = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const navigate = useNavigate();
  const [form] = Form.useForm();

  const handleSubmit = async (values: any) => {
      setIsLoading(true);

      setTimeout(() => {
        setIsLoading(false);
        navigate('/success');
      }, 2000);

  };

  return (
    <Form layout="vertical" onFinish={handleSubmit} form={form} data-aos='fade-up'>
      <Row gutter={[30, 16]}>
        <Col span={12}>
          <Col style={{ height: 206, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Typography.Title level={4}>Сумма платежа</Typography.Title>
            <Typography.Title level={3}>1000 ₸</Typography.Title>
          </Col>
          <Form.Item label="Номер карты" name="cardNumber">
            <Input placeholder="Enter card number" />
          </Form.Item>
          <Form.Item label="Владелец карты" name="cardHolder">
            <Input placeholder="Enter card holder name" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <img src="/visa.png" alt="Your image" height={200}/>
          <Form.Item label="Срок действия карты" name="cardExpiry">
            <DatePicker format="MM/YY" picker="month" style={{width: '100%'}} />
          </Form.Item>
          <Form.Item
            label={
              <span>
                CVC2/CVV код:
                <Tooltip
                  title="CVC2/CVV код — это трехзначный код, указанный на обратной стороне вашей карты."
                >
                  <QuestionCircleOutlined />
                </Tooltip>
              </span>
            }
            name="cvv"
          >
            <Input placeholder="Enter CVV code" />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Item>
            <Button type="primary" htmlType="submit" loading={isLoading}>
              Отправить
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default OrderCreateForm;
