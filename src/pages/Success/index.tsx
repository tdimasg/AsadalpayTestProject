import React from 'react';
import { Result, Button } from 'antd';
import { useNavigate } from 'react-router-dom';

const SuccessPage: React.FC = () => {
    const navigate = useNavigate();

  const handleBackToHome = () => {
    navigate('/');
};

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh', flexDirection: 'column' }}>
      <Result
        status="success"
        title="Payment Successful!"
        subTitle="Your payment has been processed successfully. Thank you for your purchase."
        extra={[
          <Button type="primary" key="home" onClick={handleBackToHome}>
            Back to Home
          </Button>,
        ]}
      />
    </div>
  );
};

export default SuccessPage;
