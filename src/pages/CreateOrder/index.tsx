import React from 'react';
import OrderCreateFrom from 'features/OrderCreate/form';

const OrderCreatePage: React.FC = () => {
  return (
    <OrderCreateFrom/>
  );
};

export default OrderCreatePage;
