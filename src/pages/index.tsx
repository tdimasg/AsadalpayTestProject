import { Routes, Route } from 'react-router';
import { lazy } from 'react';

const OrderCreate = lazy(() => import('./CreateOrder'));
const Success = lazy(() => import('./Success'));



export const Routing = () => {
  return (
    <Routes>
      <Route path="/" element={<OrderCreate />} />
      <Route path="/success" element={<Success />} />

    </Routes>
  );
};
